# UESTC Mirrors Insider Preview

## Info

Beta version of UESTC Mirrors.

## Config Files

### Site Config

- `.env`: WIP

### Build Config

- `public/data/news.json`: News preview at the index page.

### Pre-generated Config

- `src/assets/data/doc.json`: Mirrors usage and information documents.
- `src/assets/data/iso.json`: ISO download information.

## TODO

- [x] More documents
- [x] ISO Download
- [ ] News View
- [ ] Status View
- [x] Autorefresh update time and status
- [x] Dist home view
- [X] Apply theme setting to local storage
- [ ] SEO optimize
- [ ] i18n.

## Version

`Year.Month-B(eta)/S(table) (Number)`

Example: `20.09-B1`
