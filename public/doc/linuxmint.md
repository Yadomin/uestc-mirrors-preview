## Linuxmint 软件仓库镜像使用帮助

编辑 `/etc/apt/sources.list.d/official-package-repositories.list` ：

- 对于基于 Ubuntu 的原版，以 Linuxmint 19 为例：
```

deb https://mirrors.uestc.cn/linuxmint/ tara main upstream import backport
deb https://mirrors.uestc.cn/ubuntu/ bionic main restricted universe multiverse
deb https://mirrors.uestc.cn/ubuntu/ bionic-updates main restricted universe multiverse
deb https://mirrors.uestc.cn/ubuntu/ bionic-backports main restricted universe multiverse
deb https://mirrors.uestc.cn/ubuntu/ bionic-security main restricted universe multiverse
deb http://archive.canonical.com/ubuntu/ bionic partner
```
- 对于基于 Debian 的 LMDE，以 LMDE 2 为例（debian-multimedia 镜像尚未上线，请先使用 tuna 或者 ustclug 提供的镜像）：
```

deb https://mirrors.uestc.cn/linuxmint/ betsy main upstream import
deb https://mirrors.uestc.cn/debian jessie main contrib non-free
deb https://mirrors.uestc.cn/debian jessie-updates main contrib non-free
deb https://mirrors.uestc.cn/debian jessie-backports main contrib non-free
deb https://mirrors.uestc.cn/debian-security/ jessie/updates main non-free contrib
deb https://mirrors.tuna.tsinghua.edu.cn/debian-multimedia/ jessie main non-free
```
然后运行 `sudo apt-get update` 更新索引以生效。

完成后请不要再使用 mintsources（自带的图形化软件源设置工具）进行任何操作，因为在操作后，无论是否有按“确定”，mintsources 均会覆写 `/etc/apt/sources.list.d/official-package-repositories.list`

如果遇到无法拉取 https 源的情况，请先使用 http 源并安装：

```
$ sudo apt install apt-transport-https
```
