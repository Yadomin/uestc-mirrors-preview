## Debian/Ubuntu 用户

首先导入 gpg key：

```shell
$ curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
```

然后添加镜像源

``` bash
source /etc/os-release
echo "http://mirror.uestc.cn/kubernetes/apt kubernetes-${VERSION_CODENAME} main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt update
```

## RHEL/CentOS 用户

新建 `/etc/yum.repos.d/kubernetes.repo`，内容为：

```
[kubernetes]
name=kubernetes
baseurl=https://mirrros.uestc.cn/kubernetes/yum/repos/kubernetes-el7-$basearch
enabled=1
```
