import Vue from "vue";
import App from "./App.vue";
import Bus from "./Bus";
import "./registerServiceWorker";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";

import VModal from "vue-js-modal";
import VueClipboard from "vue-clipboard2";
import VueI18n from "vue-i18n";

import axios from "axios";

axios.defaults.baseURL = "/";

Vue.prototype.$ajax = axios;
Vue.prototype.Bus = Bus;

Vue.use(VModal);
VueClipboard.config.autoSetContainer = true;
Vue.use(VueClipboard);
Vue.use(VueI18n);

Vue.config.productionTip = false;

const i18n = new VueI18n({
  locale: "zh",
  messages: {
    zh: require("./assets/lang/zh.json"),
    en: require("./assets/lang/en.json"),
  },
});

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: (h) => h(App),
}).$mount("#app");
