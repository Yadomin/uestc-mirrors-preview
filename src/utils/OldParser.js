function StatusRegex(str) {
  var re = /^<a href="[a-zA-Z-]*">([a-zA-Z-]*)<\/a> *(\d{1,2}-\w*-\d{4} \d{2}:\d{2}) *(\d)/gm;
  var tmparr = {};
  var retarr = {};
  /*eslint no-console: "error"*/
  while ((tmparr = re.exec(str)) !== null) {
    retarr[tmparr[1]] = {
      status: tmparr[3] == "7" ? "success" : "syncing",
      updated: Date.parse(tmparr[2]) / 1000,
    };
  }
  return retarr;
}
export { StatusRegex };
